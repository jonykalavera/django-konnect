# coding: utf-8
from django.conf import settings

FACEBOOK_APPLICATION_ID = settings.FACEBOOK_APPLICATION_ID
FACEBOOK_APPLICATION_SECRET_KEY = settings.FACEBOOK_APPLICATION_SECRET_KEY
FACEBOOK_APPLICATION_INITIAL_PERMISSIONS = getattr(
    settings, 'FACEBOOK_APPLICATION_INITIAL_PERMISSIONS', [
        'user_about_me', 'email', 'user_photos',
        'publish_stream', 'publish_actions'
    ]
)
