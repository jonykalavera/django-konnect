# coding: utf-8
from django.conf import settings


def fbconnect(context, *args, **kwargs):
    return {
        'FACEBOOK_APPLICATION_ID': settings.FACEBOOK_APPLICATION_ID,
        'FACEBOOK_APPLICATION_NAMESPACE': settings.FACEBOOK_APPLICATION_NAMESPACE,
    }
