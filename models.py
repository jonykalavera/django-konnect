# coding:utf-8
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models


class FacebookUserManager(models.Manager):
    """Manager para los perfiles de usuario"""

    def get_current(self, uid=None, update_details=None):
        """
        Obtiene una instancia relacionado con su cuenta de Facebook.

        @type fbclient: char
        @param fbclient: ID del usuario de Facebook logueado.

        @return: Objeto de tipo FBUser
        @rtype: usuario
        """
        user, created = self.get_or_create(uid=uid)

        if update_details:
            user.first_name = update_details['first_name']
            user.last_name = update_details['last_name']

            picture = update_details.get('picture')
            if picture:
                user.picture = picture
            else:
                user.picture = \
                    "https://graph.facebook.com/%s/picture" % str(uid)

            email = update_details.get('email')
            if email:
                user.email = update_details['email']

            locale = update_details.get('locale')
            if locale:
                user.locale = update_details['locale']

            user.save()

        setattr(user, "is_new", created)
        return user


class FacebookUser(models.Model):
    """
    Modelo de usuarios Facebook.
    """
    uid = models.CharField(max_length=255, primary_key=True)
    email = models.EmailField(max_length=150, blank=True, null=True)
    first_name = models.CharField(
        max_length=50, blank=True, null=True, verbose_name="Nombre")
    last_name = models.CharField(
        max_length=50, blank=True, null=True, verbose_name="Apellido")
    picture = models.CharField(max_length=100, blank=True, null=True)
    locale = models.CharField(max_length=5, blank=True, null=True)

    objects = FacebookUserManager()

    def __unicode__(self):
        return str(self.uid)

    def html_picture(self, fbclient=None):
        """
        Devuelve una cadena en formato html con la ruta a la imágen de perfil
        del usuario de Facebook.

        @type fbclient: int
        @param fbclient: ID del usuario de Facebook logueado.

        @rtype: string
        @return: Cadena en formato html con la ruta a la imágen de perfil.
        """
        picture = self.picture
        if picture:
            return '<img src="%s" alt="" border="0" />' % (picture)
        else:
            img_tag = '<img src="%simgs/t_silhouette.jpg" alt="" border="0" />'
            return  img_tag % (settings.MEDIA_URL)
    html_picture.allow_tags = True

    def language(self):
        """
        Devuelve el idioma del usuario logueado si es que este puede obtenerse
        de otra forma regresa en (Inglés) como default.

        @rtype: string
        @return: Idioma del usuario en formato de cadena.
        """
        language = 'en'
        if self.locale:
            language = self.locale[:2]
        return language
