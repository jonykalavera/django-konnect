# coding: utf-8
import base64
from datetime import datetime, timedelta
import hashlib
import hmac
import urllib

from django.contrib.sites.models import RequestSite
from django.core.cache import cache
from django.shortcuts import redirect

import facebook
from .models import FacebookUser
from .settings import FACEBOOK_APPLICATION_ID, \
    FACEBOOK_APPLICATION_SECRET_KEY, FACEBOOK_APPLICATION_INITIAL_PERMISSIONS


import logging
logger = logging.getLogger(__name__)

try:
    import json
    _parse_json = lambda s: json.loads(s)
except ImportError:
    try:
        import simplejson
        _parse_json = lambda s: simplejson.loads(s)
    except ImportError:
        # For Google AppEngine
        from django.utils import simplejson
        _parse_json = lambda s: simplejson.loads(s)


def base64_url_decode(inp):
    padding_factor = (4 - len(inp) % 4) % 4
    inp += "=" * padding_factor
    return base64.b64decode(
        unicode(inp).translate(
            dict(zip(map(ord, u'-_'), u'+/'))))


def parse_signed_request(signed_request, secret):

    l = signed_request.split('.', 2)
    encoded_sig = l[0]
    payload = l[1]

    sig = base64_url_decode(encoded_sig)
    data = _parse_json.loads(base64_url_decode(payload))

    if data.get('algorithm').upper() != 'HMAC-SHA256':
        return None
    else:
        expected_sig = hmac.new(
            secret, msg=payload, digestmod=hashlib.sha256).digest()

    if sig != expected_sig:
        return None
    else:
        return data


def fb_connect(view):
    def nview(request, next_path=None, *args, **kwargs):
        uid = None
        domain = RequestSite(request).domain
        path_info = request.META.get('PATH_INFO')
        redirect_uri = "%(protocol)s://%(domain)s%(path)s" % {
            'protocol': request.is_secure() and 'https' or 'http',
            'domain': domain,
            'path': path_info if next_path is None else next_path,
        }
        auth_url = "http://www.facebook.com/dialog/oauth"
        auth_url += "?client_id=%s&redirect_uri=%s&locale=es_ES" % (
            FACEBOOK_APPLICATION_ID,
            redirect_uri,
        )
        perms = FACEBOOK_APPLICATION_INITIAL_PERMISSIONS
        if perms:
            auth_url += "&scope=%s" % ",".join(perms)

        setattr(request, 'auth_url', auth_url)

        code = request.REQUEST.get("code")
        if code:
            access_url = "https://graph.facebook.com/oauth/access_token?%s" % \
                urllib.urlencode({
                    'client_id': FACEBOOK_APPLICATION_ID,
                    'client_secret': FACEBOOK_APPLICATION_SECRET_KEY,
                    'redirect_uri': redirect_uri,
                    'code': code,
                    'locale': 'es_ES',
                })
            data = urllib.urlopen(access_url).read()
            if not "error" in data:
                data = dict(
                    map(lambda x: (x.split('=')[0],
                        x.split('=')[1]), data.split('&')))
                access_token = data.get('access_token')
                # number of seconds until the token expires
                expires = data.get('expires')
                expires = datetime.now() + timedelta(seconds=int(expires))
                request.session.set_expiry(expires)
            else:
                access_token = None
        else:
            access_token = request.session.get('access_token', None)
            uid = request.session.get('uid', None)
        # No importa si el access token es None. solo limita el acceso.
        graph = facebook.GraphAPI(access_token)
        user_data = None
        if uid:
            cache_key = 'user_data_%s_%s' % (
                FACEBOOK_APPLICATION_ID,
                uid,
            )
            user_data = cache.get(cache_key)
        if not user_data and access_token:
            try:
                user_data = graph.get_object('me')
            except facebook.GraphAPIError:
                try:
                    del request.session['uid']
                except KeyError:
                    pass
                try:
                    del request.session['access_token']
                except KeyError:
                    pass
                access_token = uid = None
            cache_key = 'user_data_%s_%s' % (
                FACEBOOK_APPLICATION_ID,
                user_data.get('id'),
            )
            cache.set(cache_key, user_data, 15 * 60)
        if user_data and access_token and user_data.get('id'):
            uid = user_data.get('id')
            user = FacebookUser.objects.get_current(
                uid=uid,
                update_details=user_data
            )

            logger.info("user>>>>>>>>>>>>>> %s" % user)
            request.session['uid'] = uid
            request.session['access_token'] = access_token
            request.session.modified = True
            setattr(request, 'access_token', access_token)
            setattr(request, 'fbuser', user)
            setattr(request, 'fbgraph', graph)
        else:
            logger.info("user*************")
            setattr(request, 'access_token', None)
            setattr(request, 'fbuser', None)
            setattr(request, 'fbgraph', None)
        return view(request, *args, **kwargs)
    return nview


def require_fb_connect(view):
    @fb_connect
    def nview(request, *args, **kwargs):
        if request.fbuser is None:
            return redirect(request.auth_url)
        return view(request, *args, **kwargs)
    return nview
